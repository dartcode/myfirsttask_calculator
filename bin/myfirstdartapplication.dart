import 'dart:io';

void main() {
  double num1, num2;
  String operation;

  print("Enter first number: ");
  num1 = double.parse(stdin.readLineSync()!);

  print("Enter operator (+, -, *, /): ");
  operation = stdin.readLineSync()!;

  print("Enter second number: ");
  num2 = double.parse(stdin.readLineSync()!);

  double result;

  switch (operation) {
    case '+':
      result = num1 + num2;
      break;
    case '-':
      result = num1 - num2;
      break;
    case '*':
      result = num1 * num2;
      break;
    case '/':
      if (num2 != 0) {
        result = num1 / num2;
      } else {
        print("Error: Division by zero is not allowed.");
        return;
      }
      break;
    default:
      print("Error: Invalid operator");
      return;
  }

  print("Result: $result");
}
